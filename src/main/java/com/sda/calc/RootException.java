package com.sda.calc;

public class RootException extends RuntimeException{
    public RootException(){
        super("Cant't root a negative number!");
    }

}
